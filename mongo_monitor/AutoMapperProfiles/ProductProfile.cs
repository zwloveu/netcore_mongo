﻿using System;
using System.Linq;
using System.Collections.Generic;
using AutoMapper;
using netcore_mongo.Domain;

namespace netcore_mongo.AutoMapperProfiles
{
    public class ProductProfile : Profile
    {
        public ProductProfile()
        {
            CreateMap<Product, NewProduct>();
        }
        
    }
}