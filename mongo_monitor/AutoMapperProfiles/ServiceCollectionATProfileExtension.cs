using System;
using Microsoft.Extensions.DependencyInjection;
using netcore_mongo.AutoMapperProfiles;

namespace netcore_mongo
{
    public static class ServiceCollectionATProfileExtension
    {
        public static IServiceCollection AddAutoMapperProfiles(this IServiceCollection services)
        {
            services.AddAutoMapper(typeof(ProductProfile));

            return services;
        }
    }
}