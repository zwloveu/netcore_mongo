using System;

namespace netcore_mongo.Domain
{
    public class BasicProduct
    {
        public int ProductId { get; set; }
        public long? OldProductId { get; set; }
        public string ActualWeight { get; set; }
        public string Length { get; set; }
    }
}
