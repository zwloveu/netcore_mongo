using System;

namespace netcore_mongo.Domain
{
    public class NewProduct : BasicProduct
    {
        public string UrlName { get; set; }
    }
}
