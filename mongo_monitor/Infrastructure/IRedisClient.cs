using System;
using StackExchange.Redis;

namespace netcore_mongo
{
    public interface IRedisClient
    {
        IDatabase GetDatabase();
    }
}
