﻿using StackExchange.Redis;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Reflection;
using System.Text.Json;

namespace netcore_mongo
{
    public static class ObjectExtensions
    {
        public static HashEntry[] ToHashEntry(this object data)
        {
            var properties = data.GetType()
                .GetProperties(BindingFlags.Instance | BindingFlags.Public);

            return properties.SelectMany(prop => ConvertObject(data, prop)).ToArray();
        }

        public static double ToEpochTime(this DateTime dt)
        {
            var t = dt - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            return t.TotalMilliseconds;
        }

        public static T ConvertFromRedis<T>(this HashEntry[] hashEntries)
        {
            var properties = typeof(T).GetProperties();
            var obj = Activator.CreateInstance(typeof(T));
            foreach (var property in properties)
            {
                var entry = hashEntries.FirstOrDefault(g => g.Name.ToString().Equals(property.Name));
                if (entry.Equals(new HashEntry())) continue;
                property.SetValue(obj, Convert.ChangeType(entry.Value.ToString(), property.PropertyType));
            }
            return (T)obj;
        }

        private static List<HashEntry> ConvertObject(object data, PropertyInfo prop)
        {
            var hashEntries = new List<HashEntry>();
            var value = prop.GetValue(data, null);
            if (value == null) return hashEntries;
            try
            {
                if (value is IDictionary dict)
                {
                    hashEntries.AddRange(ConvertDictionary(dict, prop.Name));
                }
                else
                {
                    var objValue = GetValue(value);
                    if (objValue != null)
                        hashEntries.Add(new HashEntry(prop.Name, RedisValue.Unbox(GetValue(value))));
                }
            }
            catch
            {
                return new List<HashEntry>();
            }

            return hashEntries;
        }

        private static IEnumerable<HashEntry> ConvertDictionary(IDictionary dict, string propName) =>
            from object key in dict.Keys
            let objValue = GetValue(dict[key])
            where objValue != null
            select new HashEntry($"{propName}{key}", RedisValue.Unbox(objValue));

        private static object GetValue(object value)
        {
            return value switch
            {
                DateTime dateTime => dateTime.ToEpochTime(),
                List<string> listString => string.Join(",", listString),
                List<int> listInt => string.Join(",", listInt),
                byte byteValue => (int)byteValue,
                short shortValue => (int)shortValue,
                bool boolValue => boolValue ? 1 : 0,
                _ => value
            };
        }
    }
}