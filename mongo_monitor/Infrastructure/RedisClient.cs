using System;
using StackExchange.Redis;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using netcore_mongo.Poco;

namespace netcore_mongo
{
    public class RedisClient : IRedisClient
    {
        private readonly ILogger<RedisClient> _Logger;
        private readonly RedisConfig _RedisConfig;
        private IDatabase _Database;

        public RedisClient(ILogger<RedisClient> logger,
            IOptions<RedisConfig> redisConfig)
        {
            this._Logger = logger ?? throw new ArgumentNullException(nameof(logger));
            this._RedisConfig = redisConfig.Value ?? throw new ArgumentNullException(nameof(redisConfig));
        }

        public IDatabase GetDatabase()
        {
            if (_Database != null) return _Database;

            var config = ConfigurationOptions.Parse(_RedisConfig.ToString());
            var conntionMultiplexer = ConnectionMultiplexer.Connect(config);
            _Database = conntionMultiplexer.GetDatabase();

            return _Database;
        }
    }
}

