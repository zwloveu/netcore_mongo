using System;
using System.Text.Json.Serialization;

namespace netcore_mongo.Models
{
    public class MongoResumeToken
    {
        [JsonPropertyName("_data")]
        public string Data { get; set; }
    }
}
