using System;

namespace netcore_mongo.Poco
{
    public sealed class MongoDBConfig
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Host { get; set; }
        public int Port { get; set; }
        public string Database { get; set; }
        public string Options { get; set; }
        public string Prefix { get; set; }

        public override string ToString()
        {
            // mongodb://username:password@host:port/database?options...
            return
                $"{Prefix}{Username}:{Password}@{Host}:{Port}/{Database}?{Options}";
        }
    }
}
