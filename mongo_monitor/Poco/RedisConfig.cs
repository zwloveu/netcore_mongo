using System;

namespace netcore_mongo.Poco
{
    public sealed class RedisConfig
    {
        public string Host { get; set; }
        public string Password { get; set; }
        public int Port { get; set; }

        public override string ToString()
        {
            // localhost:6379, password
            return
                $"{Host}:{Port},{Password}";
        }
    }
}
