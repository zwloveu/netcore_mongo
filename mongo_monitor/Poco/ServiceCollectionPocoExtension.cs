using System;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using netcore_mongo.Poco;

namespace netcore_mongo
{
    public static class ServiceCollectionPocoExtension
    {
        public static IServiceCollection AddPocos(this IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<MongoDBConfig>(configuration.GetSection("MongoDBConnection"));
            services.Configure<RedisConfig>(configuration.GetSection("RedisConnection"));

            return services;
        }
    }
}