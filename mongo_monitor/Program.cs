using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using netcore_mongo;
using Serilog;

// -------------job-----------------------------------------
CreateHostBuilder(args).Build().Run();

IHostBuilder CreateHostBuilder(string[] args)
    =>
        Host.CreateDefaultBuilder(args)
            .UseSerilog((hostBuilderContext, loggerConfiguration) => 
            {
                loggerConfiguration.ReadFrom.Configuration(new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("serilog.json", optional: false, reloadOnChange: false)
                    .Build());
            })
            // .ConfigureLogging(loggingBuilder => loggingBuilder.AddEventLog())
            .ConfigureServices((hostContext, services) =>
            {
                services.AddAutoMapperProfiles();
                services.AddPocos(hostContext.Configuration);
                services.AddRepositories();
                services.AddWorkers();
            });
// -------------job-----------------------------------------


// -------------test----------------------------------------
// var configuration =
//     new ConfigurationBuilder()
//             .SetBasePath(Directory.GetCurrentDirectory())
//             .AddJsonFile("appsettings.json", optional: false, reloadOnChange: false)
//             .Build();
// var services = new ServiceCollection()
//     .AddLogging();
// services.AddAutoMapperProfiles();
// services.AddPocos(configuration);
// services.AddRepositories();

// var builder = services.BuildServiceProvider();

// var product = new netcore_mongo.Domain.Product
// {
//     ProductId = 1,
//     OldProductId = null,
//     ActualWeight = "0.1",
//     Length = "1.1",
//     UrlName = "ThisisUrlName"
// };

// var serializer = new GroBuf.Serializer(new GroBuf.DataMembersExtracters.PropertiesExtractor(),
//     options: GroBuf.GroBufOptions.WriteEmptyObjects);

// object doubleObjValue = 0.1F;
// var rv = StackExchange.Redis.RedisValue.Unbox(doubleObjValue);
// var hashEntryWithRedisValue = new StackExchange.Redis.HashEntry("DoubleObject", rv);
// var hashEntryWithGroBufSerializedString = new StackExchange.Redis.HashEntry("DoubleObject", serializer.Serialize(doubleObjValue));
// var hashEntryWithJsonSerializedString = new StackExchange.Redis.HashEntry("DoubleObject", System.Text.Json.JsonSerializer.Serialize(doubleObjValue));

// var mapper = builder.GetRequiredService<AutoMapper.IMapper>();
// var redisRep = builder.GetRequiredService<netcore_mongo.Repositories.IRedisRep<netcore_mongo.Domain.Product>>();
// var mongoRep = builder.GetRequiredService<netcore_mongo.Repositories.IMongoRep<netcore_mongo.Domain.Product>>();

// var key = $"{product.GetType().Name}:{product.ProductId}";
// var flagProduct = mapper.Map<netcore_mongo.Domain.Product, netcore_mongo.Domain.NewProduct>(product);
// Console.WriteLine(System.Text.Json.JsonSerializer.Serialize(product));
// var hashedValue = flagProduct.ToHashEntry();
// await redisRep.UpdateAsync(key, flagProduct);

// using var cursor = await mongoRep.GetAllAsync(System.Threading.CancellationToken.None);
// while (await cursor.MoveNextAsync(System.Threading.CancellationToken.None))
// {
//     foreach (var item in cursor.Current)
//     {
//         var domainObj = item;

//         var key1 = $"{domainObj.GetType().Name}:{domainObj.ProductId}";
//         var mapData = mapper.Map<netcore_mongo.Domain.Product, netcore_mongo.Domain.NewProduct>(domainObj);
//         await redisRep.UpdateAsync(key1, mapData);
//     }
// }
// -------------test----------------------------------------


