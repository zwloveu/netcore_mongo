using netcore_mongo.Domain;
using MongoDB.Driver;
using System.Threading;
using System.Threading.Tasks;

namespace netcore_mongo.Repositories
{
    public interface IMongoRep<T> 
        where T: class, new()
    {
        Task<IAsyncCursor<ChangeStreamDocument<T>>> WatchAsync(ChangeStreamOptions options, CancellationToken cancellationToken);
        Task<IAsyncCursor<T>> GetAllAsync(CancellationToken cancellationToken);
    }
}
