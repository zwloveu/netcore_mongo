using netcore_mongo.Domain;
using System.Threading.Tasks;

namespace netcore_mongo.Repositories
{
    public interface IRedisRep<T> where T : class, new()
    {
        Task UpdateAsync(T item, string identifier);
        Task UpdateAsync(string key, object data);
        Task<TResult> GetAsync<TResult>(string key);
        Task<bool> DeleteAsync(string key);
        Task<bool> ExistsAsync(string key);
    }
}