using netcore_mongo.Domain;
using MongoDB.Bson;
using MongoDB.Driver;
using System.Threading;
using System.Threading.Tasks;

namespace netcore_mongo.Repositories
{
    public class MongoRepository<T> : IMongoRep<T> where T : class, new()
    {
        private readonly IMongoCollection<T> _collection;

        public MongoRepository(IMongoClient client, string collectionName)
        {
            _collection = client
                .GetDatabase("test") 
                .GetCollection<T>(collectionName);
        }

        public async Task<IAsyncCursor<ChangeStreamDocument<T>>> WatchAsync(ChangeStreamOptions options, CancellationToken cancellationToken)
        {
            var pipeline = new EmptyPipelineDefinition<ChangeStreamDocument<T>>()
                .Match(change => change.OperationType == ChangeStreamOperationType.Insert || change.OperationType == ChangeStreamOperationType.Update || change.OperationType == ChangeStreamOperationType.Replace);
            return await _collection.WatchAsync(pipeline, options, cancellationToken);
        }

        public async Task<IAsyncCursor<T>> GetAllAsync(CancellationToken cancellationToken)
        {
            return await _collection.FindAsync<T>(
                Builders<T>.Filter.Empty,
                new FindOptions<T>()
                {
                },
                cancellationToken);
        }
    }
}
