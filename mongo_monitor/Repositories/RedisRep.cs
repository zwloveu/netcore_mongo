
using netcore_mongo.Domain;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace netcore_mongo.Repositories
{
    public class RedisRepository<T> : IRedisRep<T> 
        where T : class, new()
    {
        private readonly IRedisClient _client;

        public RedisRepository(IRedisClient client)
        {
            _client = client;
        }

        public virtual Task UpdateAsync(T item, string identifier)
        {
            return UpdateAsync(GetKey(item, identifier), item);
        }

        public Task UpdateAsync(string key, object data)
        {
            return _client.GetDatabase().HashSetAsync(key, data.ToHashEntry());
        }

        public virtual Task<bool> DeleteAsync(string key)
        {
            return _client.GetDatabase().KeyDeleteAsync(key);
        }

        public virtual Task<bool> ExistsAsync(string key)
        {
            return _client.GetDatabase().KeyExistsAsync(key);
        }

        protected static string GetKey(T data, string identifier)
        {
            return $"{data.GetType().Name}:{identifier}";
        }

        public virtual async Task<TResult> GetAsync<TResult>(string key)
        {
            if (typeof(TResult) == typeof(string))
            {
                var redisValue = _client.GetDatabase().StringGetAsync(key);
                return (TResult)Convert.ChangeType(redisValue, typeof(TResult));
            }
            else
            {
                var hashEntries = await _client.GetDatabase().HashGetAllAsync(key);
                return hashEntries.Any() ? hashEntries.ConvertFromRedis<TResult>() : default;
            }
        }
    }
}