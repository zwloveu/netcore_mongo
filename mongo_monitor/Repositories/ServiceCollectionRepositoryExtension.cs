using System;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using netcore_mongo.Domain;
using netcore_mongo.Repositories;
using netcore_mongo.Poco;
using MongoDB.Driver;
using MongoDB.Bson.Serialization.Conventions;

namespace netcore_mongo
{
    public static class ServiceCollectionRepositoryExtension
    {
        public static IServiceCollection AddRepositories(this IServiceCollection services)
        {
            services.AddSingleton<IRedisClient, RedisClient>();
            services.AddSingleton<IMongoClient>(ctx =>
            {
                var mongoDBConfig = ctx.GetRequiredService<IOptions<MongoDBConfig>>();
                return new MongoClient(mongoDBConfig.Value.ToString());
            });

            services.AddSingleton(typeof(IMongoRep<>), typeof(MongoRepository<>));
            services.AddSingleton(typeof(IRedisRep<>), typeof(RedisRepository<>));

            services.AddSingleton<IMongoRep<Product>>(ctx =>
                new MongoRepository<Product>(ctx.GetRequiredService<IMongoClient>(), "ProductDetail"));
            services.AddSingleton<IRedisRep<Product>, RedisRepository<Product>>();

            ConventionRegistry.Register("IgnoreExtraElements",
                new ConventionPack
                {
                    // Ignore extra elements encountered during deserialization
                    new IgnoreExtraElementsConvention(true)
                },
                type => true);

            return services;
        }
    }
}