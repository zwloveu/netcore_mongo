using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using MongoDB.Bson;
using MongoDB.Driver;
using netcore_mongo.Models;
using netcore_mongo.Repositories;
using netcore_mongo.Domain;
using System.Text.Json;
using AutoMapper;

namespace netcore_mongo.Workers
{
    public class MongoStreamChangedSyncWorker : BackgroundService
    {
        private readonly ILogger<MongoStreamChangedSyncWorker> _Logger;
        private readonly IRedisRep<Product> _RedisRepository;
        private readonly IMongoRep<Product> _MongoRepository;
        private readonly IMapper _Mapper;

        public MongoStreamChangedSyncWorker(ILogger<MongoStreamChangedSyncWorker> logger,
            IRedisRep<Product> redisRepository,
            IMongoRep<Product> mongoRepository,
            IMapper mapper)
        {
            this._Logger = logger ?? throw new ArgumentNullException(nameof(logger));
            this._RedisRepository = redisRepository ?? throw new ArgumentNullException(nameof(redisRepository));
            this._MongoRepository = mongoRepository ?? throw new ArgumentNullException(nameof(mongoRepository));
            this._Mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));

        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {            
            while (!stoppingToken.IsCancellationRequested)
            {
                await Sync(stoppingToken);
                await Task.Delay(5000);// just for testing
            }
        }

        private async Task Sync(CancellationToken cancellationToken)
        {
            try
            {
                using var cursor = await _MongoRepository.GetAllAsync(cancellationToken);
                while (await cursor.MoveNextAsync(cancellationToken))
                {
                    foreach (var item in cursor.Current)
                    {
                        var domainObj = item;
                        var key = $"{domainObj.GetType().Name}:{domainObj.ProductId}";
                        var mapData = _Mapper.Map<Product, NewProduct>(domainObj);

                        await _RedisRepository.UpdateAsync(key, mapData);

                        _Logger.LogInformation($"item synced:{JsonSerializer.Serialize(domainObj)}");
                    }
                }
            }
            catch (Exception ex)
            {
                _Logger.LogError($"msg = {ex.Message}, \r\nstack={ex.StackTrace}");
            }
        }

        /*
        Change Stream feature: in replica sets and shared clusters
        This feature is not supported on a single node due to the lack of OPLOG
        */
        private async Task WatchAsync(CancellationToken cancellationToken)
        {
            try
            {
                var options = new ChangeStreamOptions
                {
                    FullDocument = ChangeStreamFullDocumentOption.UpdateLookup,
                };

                using var cursor = await _MongoRepository.WatchAsync(options, cancellationToken);
                while (await cursor.MoveNextAsync(cancellationToken))
                {
                    foreach (var item in cursor.Current)
                    {
                        var domainObj = item.FullDocument;

                        var key = $"{domainObj.GetType().Name}:{domainObj.ProductId}";
                        var mapData = _Mapper.Map<Product, NewProduct>(domainObj);
                        await _RedisRepository.UpdateAsync(key, mapData);

                        var resumeTokenExtractor = JsonSerializer.Deserialize<MongoResumeToken>(item.ResumeToken.ToJson());
                        if (resumeTokenExtractor != null)
                        {
                            await _RedisRepository.UpdateAsync(GetResumeTokenKey<Product>(), resumeTokenExtractor);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _Logger.LogError($"msg = {ex.Message}, \r\nstack={ex.StackTrace}");
            }
        }

        private string GetResumeTokenKey<T>()
        {
            return $"resumetoken:{typeof(T).Name}_test-local-0";
        }

        private async Task<MongoResumeToken> GetResumeToken()
        {
            return await _RedisRepository.GetAsync<MongoResumeToken>(GetResumeTokenKey<Product>());
        }

        private async Task DeleteResumeToken()
        {
            if (await _RedisRepository.ExistsAsync(GetResumeTokenKey<Product>()))
            {
                await _RedisRepository.DeleteAsync(GetResumeTokenKey<Product>());
            }
        }
    }
}
