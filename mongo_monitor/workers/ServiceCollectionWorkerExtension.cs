using System;
using Microsoft.Extensions.DependencyInjection;
using netcore_mongo.Workers;

namespace netcore_mongo
{
    public static class ServiceCollectionWorkerExtension
    {
        public static IServiceCollection AddWorkers(this IServiceCollection services)
        {
            services.AddHostedService<MongoStreamChangedSyncWorker>();

            return services;
        }
    }
}